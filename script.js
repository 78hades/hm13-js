let images = document.querySelectorAll(`.image-to-show `)
let buttonStop = document.createElement('button');
buttonStop.textContent = 'Припинити';
document.body.appendChild(buttonStop)
buttonStop.style.display = `none`
let buttonContinue = document.createElement('button');
buttonContinue.textContent = 'Відновити показ';
document.body.appendChild(buttonContinue)
buttonContinue.style.display = `none`
let index = 0
function showImages(){
    images.forEach((image, i) =>{ 
        if(index === i){
        image.style.display = `block`}

   else{
        image.style.display = `none`
    }
} );  index = (index + 1) % images.length;
buttonStop.style.display = `block`
buttonContinue.style.display = `block`
}
let timer
function intervalBetweenImagesStart(){
 timer = setInterval(showImages,3000)}
 intervalBetweenImagesStart()
 function intervalBetweenImagesEnd(){
     clearInterval(timer)}
   
buttonStop.addEventListener(`click`, intervalBetweenImagesEnd)
buttonContinue.addEventListener(`click`, intervalBetweenImagesStart)